
// Directivas
eWorplaceApp.directive('validEworkplaceEmail', function() {
  return {
    require: 'ngModel',
    link: function (scope, element, attrs, control) {
      control.$parsers.push(function (viewValue) {
        var newEmail = control.$viewValue;
        control.$setValidity('invalidEmail', true);
        if (typeof newEmail === 'object' || newEmail == '') return newEmail;
        if (!newEmail.match(/^[a-zA-Z0-9]+@eworkplace\.com$/)) {
          control.$setValidity('invalidEmail', false);
        }
        return viewValue;
      });
    }
  };
});