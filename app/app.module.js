'use strict';

// Creamos el app de AngularJS
// Módulos inyectados: ngRoute, toaster, ngAnimate, oitozero.ngSweetAlert
var eWorplaceApp = angular.module('eWorplaceApp', ['ngRoute', 'toaster', 'oitozero.ngSweetAlert']);