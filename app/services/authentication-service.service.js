// Services
eWorplaceApp.service('authenticationService', function() {

  this.athenticateUser = function(user, userFactory) {
    var foundUser = userFactory.filter(function(u) {
      return u.username === user.username && u.password === user.password;
    });
    return {
      isValid : foundUser.length > 0 ? true : false,
      user: foundUser.length > 0 ? foundUser[0] : foundUser
    };
  }

  this.saveLS = function(name, data) {
    localStorage.setItem(name, JSON.stringify(data));
  }

  this.getLS = function(name) {
    return JSON.parse(localStorage.getItem(name));
  }

  this.removeLS = function(name) {
    localStorage.removeItem(name);
  }

});