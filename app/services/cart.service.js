// Services
eWorplaceApp.service('cartService', function() {

  this.getProductsCart = function(productsInCart, productList) {
    var prodsInCart = [];
    const productsCounter = {};
    for (const prodIdInCart of productsInCart) {
      if (productsCounter[prodIdInCart]) {
        productsCounter[prodIdInCart] += 1;
      } else {
        productsCounter[prodIdInCart] = 1;
      }
    }
    Object.keys(productsCounter).forEach(productId => {
      console.log(productId, productsCounter[productId]);
      productList.map(function(prodInList) {
        if (prodInList.id === parseInt(productId)) {
          prodInList.quantity = productsCounter[productId];
          prodsInCart.push(prodInList);
        }
      });
    });
    return prodsInCart;
  }

});