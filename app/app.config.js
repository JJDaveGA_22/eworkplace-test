eWorplaceApp.run(function($rootScope, $location, SweetAlert, authenticationService) {

  $rootScope.productsInCart = [];

  $rootScope.$on("$locationChangeStart", function(event, next, current) {
    var auth = authenticationService.getLS('auth');
    if (!auth || !auth.isValid) {
      $location.path('/');
    }
    $rootScope.auth = auth;
  });

  $rootScope.addToCart = function(product) {
    $rootScope.productsInCart.push(product);
  }

  $rootScope.goToCart = function() {
    $location.path('/cart');
  }

  $rootScope.logout = function() {
    SweetAlert.swal({
      title: 'Desea cerrar sesión?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí',
      cancelButtonText: 'No'
    }, function(isConfirm) { 
      if (isConfirm) {
        authenticationService.removeLS('auth');
        $location.path('/');
      }
    });
  }
});