// Creamos una factoría general que nos proveerá de datos dummy
eWorplaceApp.factory('dataFactory', function() {
  // users
  var users = [
    {
      username: 'admin@eworkplace.com',
      password: 'a12345',
      role: 'admin'
    },
    {
      username: 'client@eworkplace.com',
      password: 'c12345',
      role: 'client'
    }
  ];

  // Lista de productos
  var productList = [
    {
      id: 1,
      name: 'Mouse Logitech MX 3 Master',
      description: 'Mouse profesional Logitech',
      category: 'Tecnología',
      image: 'https://picsum.photos/',
      price: 1700
    },
    {
      id: 2,
      name: 'Mouse Logitech MX 2 Master',
      description: 'Mouse profesional Logitech',
      category: 'Tecnología',
      image: 'https://picsum.photos/',
      price: 1100
    },
    {
      id: 3,
      name: 'Mouse Logitech MX 1 Master',
      description: 'Mouse profesional Logitech',
      category: 'Tecnología',
      image: 'https://picsum.photos/',
      price: 1000
    },
    {
      id: 4,
      name: 'Teclado Logitech G509',
      description: 'Teclado profesional Logitech',
      category: 'Tecnología',
      image: 'https://picsum.photos/',
      price: 2300
    },
    {
      id: 5,
      name: 'Audífonos Logitech L605',
      description: 'Audífonos profesionales Logitech',
      category: 'Tecnología',
      image: 'https://picsum.photos/',
      price: 2700
    },
    {
      id: 6,
      name: 'Laptop HP Probook',
      description: '500 GB SSD, 16 RAM, Windows 10',
      category: 'Tecnología',
      image: 'https://picsum.photos/',
      price: 18000
    },
  ];

  return {
    users,
    productList
  }
});