'use strict';

function loginController($scope, $location, toaster, dataFactory, authenticationService) {
  $scope.user = {
    username: '',
    password: ''
  };
  
  $scope.authentication = function() {
    if ($scope.frmLogin.$valid) {
      var userLogin = authenticationService.athenticateUser($scope.user, dataFactory.users);
      if (!userLogin.isValid) {
        toaster.pop({
          type: 'error',
          title: 'Error',
          body: 'Credenciales invalidas',
          showCloseButton: true
        });
        return false;
      }
      authenticationService.saveLS('auth', userLogin);
      $location.path(userLogin.user.role === 'admin' ? '/products' : '/home');
    }
  }
}