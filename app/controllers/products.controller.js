'use strict';

function productsController($scope, SweetAlert, toaster, dataFactory) {
  $scope.productList = dataFactory.productList;

  $scope.deleteProduct = function(productId) {
    SweetAlert.swal({
      title: 'Desea eliminar este producto?',
      text: 'ID producto: ' + productId,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí',
      cancelButtonText: 'No'
    }, function(isConfirm) { 
      if (isConfirm) {
        toaster.pop({
          type: 'success',
          title: 'Éxito',
          body: 'Producto ' + productId + ' eliminado',
          showCloseButton: true
        });
      }
    });
  }
}

function productFormController($scope, $location, $routeParams, toaster, dataFactory) {
  var productId = $routeParams.productId;
  $scope.action = parseInt(productId) > 0 ? 'edit' : 'new';
  $scope.product = {
    name: '',
    category: '',
    image: '',
    price: '',
    description: ''
  };
  if ($scope.action === 'edit') {
    var product = dataFactory.productList.filter(function(p) {
      return p.id === parseInt(productId);
    });
    $scope.product = product[0];
  }

  $scope.saveProduct = function() {
    if ($scope.frmProduct.$valid) {
      if ($scope.action  === 'edit') {
        toaster.pop({
          type: 'success',
          title: 'Éxito',
          body: 'Producto editado correctamente',
          showCloseButton: true
        });
      } else {
        toaster.pop({
          type: 'success',
          title: 'Éxito',
          body: 'Producto creado correctamente',
          showCloseButton: true
        });
      }
      $location.path('/products');
    }
  }
}

function productDetailController($scope, $routeParams, dataFactory) {
  var productId = $routeParams.productId;
  var product = dataFactory.productList.filter(function(p) {
    return p.id === parseInt(productId);
  });
  $scope.product = product[0];
}