'use strict';

function cartController($scope, $rootScope, dataFactory, cartService) {
  // $scope.shoppingCartList = dataFactory.shoppingCartList;
  $scope.shoppingCartList = cartService.getProductsCart($rootScope.productsInCart, dataFactory.productList);
  calculateTotal();

  $scope.removeItem = function(productId) {
    var itemsFiltered = $scope.shoppingCartList.filter(function(product){ 
      return product.id !== parseInt(productId);
    });
    $scope.shoppingCartList = itemsFiltered;
    $rootScope.productsInCart = $rootScope.productsInCart.filter(function(product){ 
      return product !== parseInt(productId);
    });
    calculateTotal();
  }

  function calculateTotal() {
    var subtotal = 0;
    var shipping = 299;
    $scope.shoppingCartList.forEach(product => {
      subtotal += product.quantity * product.price;
    });
    $scope.subtotal = subtotal;
    $scope.shipping = shipping;
    $scope.total = subtotal + shipping;
  }
}