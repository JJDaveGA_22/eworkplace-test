// Agregamos las rutas (agregamos su view y su controlador)
eWorplaceApp.config(function($routeProvider) {
  $routeProvider
    .when('/', {
      templateUrl: 'app/partials/login.partial.htm',
      controller: loginController
    })
    .when('/home', {
      templateUrl: 'app/partials/home.partial.htm',
      controller: homeController
    })
    .when('/products', {
      templateUrl: 'app/partials/products.partial.htm',
      controller: productsController,
      resolve: {
        validateAuth: function($location, authenticationService,) {
          var auth = authenticationService.getLS('auth');
          if (auth.user.role !== 'admin') {
            $location.path('/home');
          }
        }
      }
    })
    .when('/new-product', {
      templateUrl: 'app/partials/product-form.partial.htm',
      controller: productFormController
    })
    .when('/edit-product/:productId', {
      templateUrl: 'app/partials/product-form.partial.htm',
      controller: productFormController
    })
    .when('/product-detail/:productId', {
      templateUrl: 'app/partials/product-detail.partial.htm',
      controller: productDetailController
    })
    .when('/cart', {
      templateUrl: 'app/partials/cart.partial.htm',
      controller: cartController
    })
    .otherwise({
      redirectTo: '/'
    });
});
